import 'package:flutter/material.dart';
import 'package:mobxtodo/pages/task_list_page.dart';

void main() => runApp(TodoListPage());

class TodoListPage extends StatefulWidget {
  @override
  _TodoListPageState createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      home: TaskListPage(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mobxtodo/data/model/task.dart';

class AddTaskPage extends StatefulWidget {
  @override
  _AddTaskPageState createState() => _AddTaskPageState();
}

class _AddTaskPageState extends State<AddTaskPage> {
  TextEditingController titleController = TextEditingController();

  TextEditingController descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: titleController,
                decoration: InputDecoration(hintText: 'Enter title'),
              ),
              TextField(
                controller: descController,
                decoration: InputDecoration(hintText: 'Enter description'),
              ),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(
                        context,
                        Task(
                          title: titleController.text,
                          description: descController.text,
                        ));
                  },
                  child: Text(
                    'Add',
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:mobxtodo/data/model/task.dart';
import 'package:mobxtodo/data/task_repository.dart';
import 'package:mobxtodo/pages/add_task_page.dart';
import 'package:mobxtodo/state/task_store.dart';

class TaskListPage extends StatefulWidget {
  @override
  _TaskListPageState createState() => _TaskListPageState();
}

class _TaskListPageState extends State<TaskListPage> {
  TaskStore _taskStore = TaskStore(TaskRepositoryImpl());
  List<ReactionDisposer> _disposers;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _taskStore.getTaskList();
  }

  @override
  void dispose() {
    _disposers.forEach((d) => d());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => AddTaskPage(),
            ),
          );
          print(result);
          if (result != null) {
            _taskStore.addTask(result);
          }
        },
      ),
      appBar: AppBar(
        title: Text('Tasklist'),
      ),
      body: Observer(builder: (_) {
        switch (_taskStore.state) {
          case StoreState.initial:
            return buildLoadingState();
            break;
          case StoreState.loading:
            return buildLoadingState();
            break;
          case StoreState.loaded:
            return buildLoadedState(_taskStore.taskList);
            break;
          default:
            return Container();
        }
      }),
    );
  }

  Widget buildLoadingState() {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: Colors.blueGrey,
      ),
    );
  }

  Widget buildLoadedState(List<Task> taskList) {
    return ListView.separated(
      itemBuilder: (BuildContext context, int index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              taskList[index].title,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
            Text(
              taskList[index].description,
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey,
              ),
            )
          ],
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemCount: taskList.length,
    );
  }
}

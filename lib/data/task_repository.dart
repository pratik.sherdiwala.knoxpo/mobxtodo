import 'package:mobxtodo/data/model/task.dart';

abstract class TaskRepository {
  Future<List<Task>> getTaskList();
  Future<bool> addTask(Task task);
}

class TaskRepositoryImpl implements TaskRepository {
  List<Task> taskList = [
    Task(title: 'Hello', description: 'hello'),
    Task(title: 'Hello', description: 'hello'),
    Task(title: 'Hello', description: 'hello'),
    Task(title: 'Hello', description: 'hello'),
    Task(title: 'Hello', description: 'hello'),
  ];

  @override
  Future<List<Task>> getTaskList() {
    return Future.delayed(
      Duration(seconds: 2),
      () {
        return taskList;
      },
    );
  }

  @override
  Future<bool> addTask(Task task) {
    final tempList = [];

    taskList.forEach((task) {
      tempList.add(task);
    });

    return Future.delayed(
      Duration(seconds: 2),
      () {
        tempList.add(task);
        taskList = tempList;
        return true;
      },
    );
  }
}

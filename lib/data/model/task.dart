import 'package:flutter/material.dart';

class Task {
  final String title;
  final String description;

  Task({
    @required this.title,
    @required this.description,
  });

  @override
  bool operator ==(other) {
    if (identical(this, other)) return true;

    return other is Task &&
        other.title == title &&
        other.description == description;
  }

  @override
  int get hashCode => title.hashCode ^ description.hashCode;
}

class NoParams {
  @override
  bool operator ==(other) {
    return true;
  }

  @override
  int get hashCode => super.hashCode;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TaskStore on _TaskStore, Store {
  Computed<StoreState> _$stateComputed;

  @override
  StoreState get state =>
      (_$stateComputed ??= Computed<StoreState>(() => super.state)).value;

  final _$_taskListFutureAtom = Atom(name: '_TaskStore._taskListFuture');

  @override
  ObservableFuture<List<Task>> get _taskListFuture {
    _$_taskListFutureAtom.context.enforceReadPolicy(_$_taskListFutureAtom);
    _$_taskListFutureAtom.reportObserved();
    return super._taskListFuture;
  }

  @override
  set _taskListFuture(ObservableFuture<List<Task>> value) {
    _$_taskListFutureAtom.context.conditionallyRunInAction(() {
      super._taskListFuture = value;
      _$_taskListFutureAtom.reportChanged();
    }, _$_taskListFutureAtom, name: '${_$_taskListFutureAtom.name}_set');
  }

  final _$taskListAtom = Atom(name: '_TaskStore.taskList');

  @override
  List<Task> get taskList {
    _$taskListAtom.context.enforceReadPolicy(_$taskListAtom);
    _$taskListAtom.reportObserved();
    return super.taskList;
  }

  @override
  set taskList(List<Task> value) {
    _$taskListAtom.context.conditionallyRunInAction(() {
      super.taskList = value;
      _$taskListAtom.reportChanged();
    }, _$taskListAtom, name: '${_$taskListAtom.name}_set');
  }

  final _$getTaskListAsyncAction = AsyncAction('getTaskList');

  @override
  Future getTaskList() {
    return _$getTaskListAsyncAction.run(() => super.getTaskList());
  }

  final _$addTaskAsyncAction = AsyncAction('addTask');

  @override
  Future addTask(Task task) {
    return _$addTaskAsyncAction.run(() => super.addTask(task));
  }
}

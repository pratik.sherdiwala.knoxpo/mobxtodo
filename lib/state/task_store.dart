import 'package:mobx/mobx.dart';
import 'package:mobxtodo/data/model/task.dart';
import 'package:mobxtodo/data/task_repository.dart';

part 'task_store.g.dart';

class TaskStore extends _TaskStore with _$TaskStore {
  TaskStore(TaskRepository taskRepository) : super(taskRepository);
}

enum StoreState {
  initial,
  loading,
  loaded,
}

abstract class _TaskStore with Store {
  final TaskRepository _taskRepository;

  _TaskStore(this._taskRepository);

  @observable
  ObservableFuture<List<Task>> _taskListFuture;

  @observable
  List<Task> taskList;

  @computed
  StoreState get state {
    if (_taskListFuture == null) {
      return StoreState.initial;
    }
    return _taskListFuture.status == FutureStatus.pending
        ? StoreState.loading
        : StoreState.loaded;
  }

  @action
  Future getTaskList() async {
    _taskListFuture = ObservableFuture(_taskRepository.getTaskList());
    taskList = await _taskListFuture;
  }

  @action
  Future addTask(Task task) async {
    taskList.add(task);
  }
}
